package ch.adrian.brunner;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author adrian
 * date 10.7.2020
 *
 */
@SpringBootApplication
public class JwtApplication {

	 @Bean
	    public BCryptPasswordEncoder bCryptPasswordEncoder() {
	        return new BCryptPasswordEncoder();
	    }
	
	public static void main(String[] args) {
		SpringApplication.run(JwtApplication.class, args);
	}

}
