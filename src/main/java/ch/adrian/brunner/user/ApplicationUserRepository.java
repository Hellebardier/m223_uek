package ch.adrian.brunner.user;


import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author adrian
 * date 10.7.2020
 *
 */

public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {
    ApplicationUser findByUsername(String username);
}