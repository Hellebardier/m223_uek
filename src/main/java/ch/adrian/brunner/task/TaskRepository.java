package ch.adrian.brunner.task;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author adrian
 * date 10.7.2020
 *
 */

public interface TaskRepository extends JpaRepository<Task, Long> {
}